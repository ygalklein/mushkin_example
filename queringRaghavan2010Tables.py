from astroquery.vizier import Vizier

Vizier.ROW_LIMIT = -1


catalog_list = Vizier.find_catalogs("A Survey of Stellar Families")
print(catalog_list)

catalog_list = {k: v for k, v in catalog_list.items() if "J/ApJS/190/1" in k}
print(catalog_list)

catalogs = Vizier.get_catalogs(catalog_list.keys())

print(catalogs)
